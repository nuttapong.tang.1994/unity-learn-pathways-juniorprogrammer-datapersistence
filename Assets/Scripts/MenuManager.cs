using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class MenuManager : MonoBehaviour
{
    public TextMeshProUGUI bestScoreText;

    private void Start()
    {
        if (GameInstance.Instance.bestScoreData.name != "")
        {
            SetBestScoreText(GameInstance.Instance.bestScoreData);
        }
    }

    public void SetBestScoreText(ScoreData scoreData)
    {
        bestScoreText.text = "Best Score : " + scoreData.name + " : " + scoreData.score;
    }

    public void SetPlayerName(string inputText)
    {
        GameInstance.Instance.currentName = inputText;
    }

    public void StartGame()
    {
        if (GameInstance.Instance.currentName != "")
            SceneManager.LoadScene(1);
    }

    public void ExitGame()
    {
#if UNITY_EDITOR
        EditorApplication.ExitPlaymode();
#else
        Application.Quit(); // original code to quit Unity player
#endif
    }
}
