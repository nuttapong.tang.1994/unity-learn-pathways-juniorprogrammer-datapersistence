[System.Serializable]
public class ScoreData
{
    public string name;
    public int score;

    public ScoreData()
    { }

    public ScoreData(string name, int score)
    {
        this.name = name;
        this.score = score;
    }
}
