using UnityEngine;

public class PowerBrick : Brick
{
    [SerializeField]
    private Ball ballPrefab;
    private bool isSpawnBall = false;

    protected override void OnCollisionEnter(Collision other)
    {
        isSpawnBall = true;
        base.OnCollisionEnter(other);
    }

    protected override Color GetBlockBaseColor()
    {
        return Color.red;
    }

    private void OnDestroy()
    {
        if (!isSpawnBall)
            return;
        isSpawnBall = false;

        MainManager mainManager = GameObject.Find("MainManager").GetComponent<MainManager>();
        mainManager.OnBallSpawn();
        Ball ball = Instantiate(ballPrefab, transform.position, Quaternion.identity);

        float random = Random.Range(0f, 360f);
        Vector3 forceDir = new Vector3(Mathf.Cos(random), Mathf.Sin(random), 0);
        Rigidbody ballRd = ball.GetComponent<Rigidbody>();
        if (ballRd)
            ballRd.AddForce(forceDir * 2.0f, ForceMode.VelocityChange);
    }
}
