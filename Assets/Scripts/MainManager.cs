using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainManager : MonoBehaviour
{
    public Brick BrickPrefab;
    public PowerBrick PowerBrickPrefab;
    public int LineCount = 6;
    public Rigidbody Ball;

    public Text ScoreText;
    public Text BestScoreText;
    public GameObject GameOverText;

    private bool m_Started = false;
    private int m_Points;

    private int ballCount = 1;
    private bool m_GameOver = false;


    // Start is called before the first frame update
    void Start()
    {
        const float step = 0.6f;
        int perLine = Mathf.FloorToInt(4.0f / step);

        int[] pointCountArray = new[] { 1, 1, 2, 2, 5, 5 };
        for (int i = 0; i < LineCount; ++i)
        {
            for (int x = 0; x < perLine; ++x)
            {
                Brick prefab = BrickPrefab;
                int point = pointCountArray[i];
                if (i == 0 && x == 1
                    || i == 0 && x == 4)
                {
                    prefab = PowerBrickPrefab;
                    point = 0;
                }

                Vector3 position = new Vector3(-1.5f + step * x, 2.5f + i * 0.3f, 0);
                var brick = Instantiate(prefab, position, Quaternion.identity);
                brick.PointValue = point;
                brick.onDestroyed.AddListener(AddPoint);
            }
        }

        if (GameInstance.Instance.bestScoreData.name != "")
        {
            SetBestScoreText(GameInstance.Instance.bestScoreData);
        }
    }

    private void Update()
    {
        if (!m_Started)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                m_Started = true;
                float randomDirection = Random.Range(-1.0f, 1.0f);
                Vector3 forceDir = new Vector3(randomDirection, 1, 0);
                forceDir.Normalize();

                Ball.transform.SetParent(null);
                Ball.AddForce(forceDir * 2.0f, ForceMode.VelocityChange);
            }
        }
        else if (m_GameOver)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
    }

    void AddPoint(int point)
    {
        m_Points += point;
        ScoreText.text = $"Score : {m_Points}";
    }

    public void SetBestScoreText(ScoreData scoreData)
    {
        BestScoreText.text = "Best Score : " + scoreData.name + " : " + scoreData.score;
    }

    public void OnBallSpawn()
    {
        ballCount += 1;
    }

    public void OnBallDestroy()
    {
        ballCount -= 1;
        if (ballCount <= 0)
            GameOver();
    }

    public void GameOver()
    {
        m_GameOver = true;
        GameOverText.SetActive(true);

        if (m_Points > GameInstance.Instance.bestScoreData.score)
        {
            ScoreData scoreData = new ScoreData(GameInstance.Instance.currentName, m_Points);
            SetBestScoreText(scoreData);
            GameInstance.Instance.bestScoreData = scoreData;
            GameInstance.Instance.SaveBestScoreData();
        }
    }
}
