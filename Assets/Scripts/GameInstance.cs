using System.IO;
using UnityEngine;

public class GameInstance : MonoBehaviour
{
    public static GameInstance Instance;

    public ScoreData bestScoreData;
    public string currentName = "";

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
        LoadBestScoreData();
    }

    public void SaveBestScoreData()
    {
        string json = JsonUtility.ToJson(bestScoreData);
        File.WriteAllText(Application.persistentDataPath + "/savefile.json", json);
    }

    public void LoadBestScoreData()
    {
        string path = Application.persistentDataPath + "/savefile.json";
        if (File.Exists(path))
        {
            string json = File.ReadAllText(path);
            bestScoreData = JsonUtility.FromJson<ScoreData>(json);
        }
    }
}
