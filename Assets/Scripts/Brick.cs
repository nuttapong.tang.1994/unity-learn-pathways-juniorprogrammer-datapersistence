using UnityEngine;
using UnityEngine.Events;

public class Brick : MonoBehaviour
{
    public UnityEvent<int> onDestroyed;

    protected int _pointValue;

    public int PointValue
    {
        get { return _pointValue; }
        set
        {
            _pointValue = value;
            UpdateBlockColor();
        }
    }

    protected virtual void OnCollisionEnter(Collision other)
    {
        onDestroyed.Invoke(_pointValue);

        //slight delay to be sure the ball have time to bounce
        Destroy(gameObject, 0.2f);
    }

    private void UpdateBlockColor()
    {
        var renderer = GetComponentInChildren<Renderer>();
        MaterialPropertyBlock block = new MaterialPropertyBlock();
        block.SetColor("_BaseColor", GetBlockBaseColor());
        renderer.SetPropertyBlock(block);
    }

    protected virtual Color GetBlockBaseColor()
    {
        switch (_pointValue)
        {
            case 1:
                return Color.green;
            case 2:
                return Color.yellow;
            case 5:
                return Color.blue;
        }

        return Color.white;
    }
}
